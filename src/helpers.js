export const helpers = {
    data() {
        return {
            notes: [],
            note: {
                id: 0,
                title: "",
                text: "",
                archived: false,
                color: "#fff"
            },
            addNoteMsg: {
                message: "",
                timeout: 2000,
                actionText: "Undo"
            },
            resource: {}
        }
    },
    methods: {
        getMethod() {
            this.resource.get()
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    const resultArray = [];
                    for (let key in data) {
                        resultArray.push(data[key]);
                    }
                    this.notes = resultArray;
                });
        },
        showSnack() {
            const snackbarContainer = document.querySelector(
                "#demo-snackbar-example");
            componentHandler.upgradeElement(snackbarContainer);
            snackbarContainer.MaterialSnackbar.showSnackbar(
                this.addNoteMsg);
        },
        deleteNote(note) {
            this.note.id = note.id;
            this.resource
                .get("")
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    const resultArray = [];
                    for (let key in data) {
                        resultArray.push(data[key]);
                        if (data[key].id == this.note.id) {
                            this.$http
                                .delete(
                                    "https://mon-book.firebaseio.com/data/" + key + ".json",
                                    note
                                )
                                .then(
                                    response => {
                                        this.addNoteMsg.message = "Note deleted";
                                        this.showSnack();
                                        this.getMethod();
                                    },
                                    error => {
                                        console.log(error);
                                    }
                                );
                        }
                    }
                    this.notes = resultArray;
                });
        },
        // Affiche la couleur des cartes lors du chargement des pages notes et archives
        getColorOnLoad() {
            this.resource
                .get()
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    const resultArray = [];
                    for (let key in data) {
                        resultArray.push(data[key]);
                        const cards = document.querySelectorAll(".color");
                        // set l'id correspondant aux cartes et définit leur bgcolor s'ils en ont une
                        for (let i = 0; i < cards.length; i++) {
                            if (data[key].id == this.notes[i].id) {
                                cards[i].setAttribute("id", data[key].id);
                                cards[i].style.backgroundColor = this.notes[i].color;
                            }
                        }
                    }
                    this.notes = resultArray;
                });
        },
        test() {
            this.resource
                .get()
                .then(response => {
                    return response.json();
                })
                .then(data => {
                    const resultArray = [];
                    const cards = document.getElementsByClassName("color");
                    for (let key in data) {
                        resultArray.push(data[key]);

                        // set l'id correspondant aux cartes et définit leur bgcolor s'ils en ont un
                        for (let i = 0; i < cards.length; i++) {
                            if (data[key].id == this.notes[i].id) {
                                cards[i].setAttribute("id", data[key].id);
                                cards[i].style.backgroundColor = this.notes[i].color;
                            }
                        }
                    }
                    this.notes = resultArray;
                });
        }
    }
}