import Archives from "./components/Archives.vue";
import Home from './components/Home.vue';

export const routes = [
    {path: '', component: Home, name: 'home'},
    {path: '/archives', component: Archives, name: 'archives'}, 
    {path: '*', redirect: '/'}
]