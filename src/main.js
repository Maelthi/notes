import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import {routes} from './routes';


Vue.use(VueResource);
Vue.use(VueRouter);
const router = new VueRouter({
  routes
});
Vue.http.options.root = 'https://mon-book.firebaseio.com/';


export const eventBus = new Vue ({
  methods: {
    isCreated() {
      this.$emit('noteCreated');      
    }, 
    isArchived(id) {
      this.$emit('noteArchived', id);
    }
  }
});

new Vue({
  el: '#app',
  router: router,
  render: h => h(App)
})
